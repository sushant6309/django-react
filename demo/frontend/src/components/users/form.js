import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { addUser } from "../../actions/users";

export class Form extends Component {
  state = {
    userName: "",
    email: "",
    firstName: "",
    lastName: "",
    password: ""
  };

  static propTypes = {
    addUser: PropTypes.func.isRequired
  };

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  onSubmit = e => {
    e.preventDefault();
    const { userName, email, firstName, lastName, password } = this.state;
    const user = { userName, email, firstName, lastName, password };
    this.props.addUser(user);
    this.setState({
      userName: "",
      email: "",
      firstName: "",
      lastName: "",
      password: ""
    });
  };

  render() {
    const { userName, email, firstName, lastName, password } = this.state;
    return (
      <div className="card card-body mt-4 mb-4">
        <h2>Add User</h2>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>User Name</label>
            <input
              className="form-control"
              type="text"
              name="userName"
              onChange={this.onChange}
              value={userName}
            />
          </div>
          <div className="form-group">
            <label>Email</label>
            <input
              className="form-control"
              type="email"
              name="email"
              onChange={this.onChange}
              value={email}
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input
              className="form-control"
              type="password"
              name="password"
              onChange={this.onChange}
              value={password}
            />
          </div>
          <div className="form-group">
            <label>First Name</label>
            <input
              className="form-control"
              type="text"
              name="firstName"
              onChange={this.onChange}
              value={firstName}
            />
          </div>
          <div className="form-group">
            <label>Last Name</label>
            <textarea
              className="form-control"
              type="text"
              name="lastName"
              onChange={this.onChange}
              value={lastName}
            />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default connect(
  null,
  { addUser }
)(Form);