import axios from "axios";
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "XCSRF-TOKEN";
import { createMessage, returnErrors } from "./messages";

import { GET_USERS, DELETE_USER, ADD_USER } from "./types";

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

function userPythonTransformer(user) {
  return {
    id: user.app_user_id,
    userName: user.app_user_login,
    firstName: user.app_user_first_name,
    lastName: user.app_user_last_name,
    email: user.app_user_email,
    liveFrom: user.create_date
  }
}

function userReactTransformer(user) {
  return {
    app_user_id: user.id,
    app_user_login: user.userName,
    app_user_first_name: user.firstName,
    app_user_last_name: user.lastName,
    app_user_email: user.email,
    app_user_password: user.password
  }
}

// GET USERS
export const getUsers = () => (dispatch, getState) => {
  axios
    .get("/api/users/", {})
    .then(res => {

      dispatch({
        type: GET_USERS,
        payload: res.data.map(user => userPythonTransformer(user))
      });
    })
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

// DELETE USER
export const deleteUser = id => (dispatch, getState) => {
  axios
    .delete(`/api/users/${id}/`, {
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": readCookie("csrftoken")
      }
    })
    .then(res => {
      dispatch(createMessage({ deleteUSER: "User Deleted" }));
      dispatch({
        type: DELETE_USER,
        payload: id
      });
    })
    .catch(err => console.log(err));
};

// ADD USER
export const addUser = USER => (dispatch, getState) => {
  axios
    .post("/api/users/", userReactTransformer(USER), {
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": readCookie("csrftoken")
      }
    })
    .then(res => {
      dispatch(createMessage({ addUSER: "USER Added" }));
      dispatch({
        type: ADD_USER,
        payload: userPythonTransformer(res.data)
      });
    })
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};