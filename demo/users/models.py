from django.db import models


# Create your models here.

class Users(models.Model):
    app_user_id = models.AutoField(primary_key=True)
    app_user_login = models.CharField(max_length=256)
    app_user_first_name = models.CharField(max_length=256)
    app_user_last_name = models.CharField(max_length=256)
    app_user_email = models.EmailField(max_length=100, unique=True)
    create_date = models.DateTimeField(auto_now_add=True)
    app_user_password = models.CharField(max_length=256)

    def __str__(self):
        return self.app_user_first_name
