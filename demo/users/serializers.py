from rest_framework import serializers
from . models import Users

class usersSerialzer(serializers.ModelSerializer):

    class Meta:
        model = Users
        fields = (
            'app_user_id',
            'app_user_login',
            'app_user_first_name',
            'app_user_last_name',
            'app_user_email',
            'create_date'
        )
