# Django React

Demo Project to implement react with django in monolithic architecture.

## Installation

Clone the git repository

```bash
 git clone git@gitlab.com:sushant6309/django-react.git
```
Install pip env and install python dependencies of project.

```bash
pip3 install pipenv
pipenv shell
pipenv install

```

Install node dependencies
```bash
npm i
npm run dev

```

Steps to initiate Django Project
```bash
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py createsuperuser
python3 manage.py runserver

```


## License
[MIT](https://choosealicense.com/licenses/mit/)